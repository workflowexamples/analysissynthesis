# analysis base version from which we start
FROM atlas/analysisbase:21.2.125

# put the code into the repo with the proper owner
COPY --chown=atlas . /code/src

# note that the build directory already exists in /code/src
RUN sudo chown -R atlas /code/src 
    
# where you are left after booting the image
WORKDIR /code/src
