# Description
This is an example repo that represents the second stage of analysis which typically
includes drawing histograms or performing statistical analysis.  

# Usage
This is intended to be built upon the `analysisbase:21.2.125` Docker
image.  However, both of the scripts are python-based so are used directly without 
compilation.  Start by setting it up :
```
docker run --rm -it -v $PWD:$PWD atlas/analysisbase:21.2.125
```
and navigating to the directory where you cloned this repository. Once 
there, setup the release and then use either of the scripts
```
source /home/atlas/release_setup.sh
```

# Available Scripts
The usage of both scripts can be found by their self-documented help menus with the `-h` 
command line argument.


## `AddHists.py`
This script adds together two histograms stored within two root files.  It is like
`hadd` but in a targetted way.  In a real analysis, you probably would not use this, but
this is just for illustration.

## DrawHist.py
This script draws a histogram stored in a root file.